<?php

namespace App\Http\Controllers;

use App\Rutina;
use App\Http\Requests;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

use Request;

class RutinasControlador extends Controller
{
    public function index ()
    {
    	$rutinas = Rutina::latest()->get();

    	return view('rutinas.index', compact('rutinas'));
    }

    public function show (Rutina $rutina)
    {

    	return view('rutinas.show', compact('rutina'));

    }

    public function create () 
    {

    	return view('rutinas.create');

    }

    public function store (Requests\RutinaRequest $request)
    {

    	Rutina::create($request->all());

    	return redirect('rutinas');

    }

    public function edit ($idRutina)
    {

    	$rutina = Rutina::findOrFail($idRutina);

    	return view ('rutinas.edit', compact('rutina'));

    }

    public function update ($idRutina, RutinaRequest $request)
    {

    	$rutina = Rutina::findOrFail($idRutina);

    	$rutina->update($request->all());

    	return redirect('rutinas');

    }


}
