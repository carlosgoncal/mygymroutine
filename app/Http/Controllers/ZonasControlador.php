<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Zona;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ZonasControlador extends Controller
{
    public function index()
    {

    	$zonas = Zona::all();

    	return view('zonas.index', compact('zonas'));

    }

    public function show(Zona $zona)
    {

    	return view('zonas.show', compact('zona'));

    }
}
