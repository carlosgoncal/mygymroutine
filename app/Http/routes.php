<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('zonas', 'ZonasControlador@index');
Route::get('zonas/{zona}', 'ZonasControlador@show');




/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    //Rutas creadas manualmente - optimizamos el procreso con resource del controlador

    Route::get('rutinas', 'RutinasControlador@index');
	Route::get('rutinas/crear', 'RutinasControlador@create');
	Route::get('rutinas/{rutina}', 'RutinasControlador@show');
	Route::post('rutinas', 'RutinasControlador@store');
	Route::get('rutinas/{rutina}/edit', 'RutinasControlador@edit');

	Route::put('rutinas{rutina', 'RutinasControlador@update');

	

	//Route::resource('rutinas', 'RutinasControlador');

});
