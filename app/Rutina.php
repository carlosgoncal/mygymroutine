<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rutina extends Model
{
    protected $fillable = [
    	'nombre',
    	'frequencia',
    	'objetivo',
    	'dificultad',
    	'descripcion',
    	'tags',
    	'fechaPublicacion'
    ];
}
