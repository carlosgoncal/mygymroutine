<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ejercicio extends Model
{

	protected $fillable = ['nombre', 'descripcion'];


    public function zona()
    {
    	return $this->belongsTo(Zona::class);
    }
}
