<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zona extends Model
{
    public function ejercicios()
    {
    	return $this->hasMany(Ejercicio::class);
    }

}
