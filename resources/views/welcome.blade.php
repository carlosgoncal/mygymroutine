@extends('layout')


@section('customcss')

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 96px;
        }
    </style>

@stop
    
@section('content')

    <div class="container">
        <div class="content">
            <div class="title">My GymRoutine App</div>
            <ol class="breadcrumb">
          <li><a href="/">Inicio</a></li>
          <li><a href="zonas">Zonas a ejercitar</a></li>
          <li><a href="rutinas">Rutinas</a></li>
        </ol>
        </div>
    </div>

@stop
