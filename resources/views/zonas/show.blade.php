@extends('layout')

@section('content')

	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<ol class="breadcrumb">
			  <li><a href="/">Inicio</a></li>
			  <li><a href="/zonas/">Zonas a ejercitar</a></li>
			  <li class="active">Ejercicios {{ $zona->nombre }}</li>
			</ol>
			<div class="page-header">
			  <h1>{{ $zona->nombre }} <small>Ejercicios</small></h1>
			</div>


			<div class="list-group">
				@foreach ($zona->ejercicios as $ejercicio)

					<div class="list-group-item active">
						{{ $ejercicio->nombre }}
					</div>
					<div class="list-group-item">
						- {{ $ejercicio->descripcion }}
					</div>

				@endforeach

		</div>
	</div>

@stop