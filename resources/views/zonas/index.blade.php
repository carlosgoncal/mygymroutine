@extends('layout')

@section('content')

	<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<ol class="breadcrumb">
		  <li><a href="/">Inicio</a></li>
		  <li class="active">Zonas a ejercitar</a></li>
		</ol>
		<div class="page-header">
		  <h1>Zonas a ejercitar</h1>
		</div>


		<div class="list-group">
			<div  class="list-group-item active">
			    Elije tu zona
			</div>

			@foreach ($zonas as $zona)

				
					<a href="/zonas/{{ $zona->id }}" class="list-group-item">{{ $zona->nombre}}</a>
				

			@endforeach

		</div>
	</div>
	</div>

@stop