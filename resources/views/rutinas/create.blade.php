@extends('layout')

@section('content')

	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="page-header">
			  <h1>Crea tu rutina</h1>
			</div>
		
			{!! Form::open(['url' => 'rutinas']) !!}
				<div class="form-group">
				{!! Form::label('nombre', 'Nombre:') !!}
				{!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Rutina definición verano 2016']) !!}
				</div>
				<div class="form-group">
				{!! Form::label('frequencia', 'Frecuencia:') !!}
				{!! Form::select('frequencia', array('1' => '1 Día', '2' => '2 Días', '3' => '3 Días', '4' => '4 Días', '5' => '5 Días', '6' => '6 Días', '7' => '7 Días'), '1', ['class' => 'form-control']); !!}
				</div>
				<div class="form-group">				
				{!! Form::label('objetivo', 'Objetivo:') !!}
				{!! Form::text('objetivo', null, ['class' => 'form-control', 'placeholder' => 'Volumen, Definición, Matenimiento, etc.']) !!}
				</div>
				<div class="form-group">
				{!! Form::label('dificultad', 'Dificultad:') !!}
				{!! Form::select('dificultad', array('Principiante' => 'Principiante  (0-6 meses entrenando)', 'Intermedio' => 'Intermedio  (7-18 meses entrenando)', 'Experto' => 'Experto'), 'Principiante', ['class' => 'form-control']); !!}
				</div>
				<div class="form-group">
				{!! Form::label('descripcion', 'Descripcion:') !!}
				{!! Form::textarea('descripcion', null, ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
				{!! Form::label('tags', 'Tags:') !!}
				{!! Form::text('tags', null, ['class' => 'form-control', 'placeholder' => '#volumen  #suplementación  #arnold']) !!}
				</div>
				<div class="form-group">
				{!! Form::submit('Crear rutina', ['class' => 'btn btn-primary form-control']) !!}
				</div>

			{!! Form::close() !!}


			@if ($errors->any())

				<ul class="alert alert-danger">
					@foreach ($errors->all() as $error)
						<li> {{ $error }}</li>
					@endforeach
				</ul>
			@endif
			</div>	
		</div>
	</div>


@stop