@extends('layout')

@section('content')

	<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<ol class="breadcrumb">
		  <li><a href="/">Inicio</a></li>
		  <li class="active">Rutinas</a></li>
		</ol>
		<div class="page-header">
		  <h1>Rutinas disponibles</h1>
		</div>


		<div class="list-group">
			<div  class="list-group-item active">
			    Elije tu rutina
			</div>

			@foreach ($rutinas as $rutina)

				
					<a href="/rutinas/{{ $rutina->id }}" class="list-group-item">{{ $rutina->nombre}}</a>
				

			@endforeach

		</div>
	</div>
	</div>

@stop