@extends('layout')

@section('content')

	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<ol class="breadcrumb">
			  <li><a href="/">Inicio</a></li>
			  <li><a href="/rutinas/">Listado de Rutinas</a></li>
			  <li class="active">{{ $rutina->nombre }}</li>
			</ol>
			<div class="page-header">
			  <h1>{{ $rutina->nombre }} <small>Ejercicios</small></h1>
			</div>
			<article>
				<p>Frecuencia: {{ $rutina->frequencia }} día/s</p>
				<p>Objetivo: {{ $rutina->objetivo }}</p>
				<p>Dificultad: {{ $rutina->dificultad }}</p>
				<p>Descripcion: {{ $rutina->descripcion }}</p>
				<p>Tags: {{ $rutina->tags }}</p>

			</article>

	</div>

@stop